package utils

import (
	"crypto/md5"
	"fmt"
	"math/rand"
)

var characterRunes = []rune("abcdefghijklmnopqueryFormrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")

// 生成随机md5 32
func GenerateRandomMd5(n ...int) string {
	noRandomCharacters := 32
	if len(n) > 0 {
		noRandomCharacters = n[0]
	}
	randString := GenerateRandomStr(noRandomCharacters)
	Md5Inst := md5.New()
	Md5Inst.Write([]byte(randString))
	bs := Md5Inst.Sum([]byte(""))
	return fmt.Sprintf("%x", bs)
}

// 生成随机字符串 32
func GenerateRandomStr(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = characterRunes[rand.Intn(len(characterRunes))]
	}
	return string(b)
}
