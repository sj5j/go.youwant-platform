# go.youwant-platform

> 一个简单的服务型应用

服务运行测试

1. 执行 `go run .`，使用浏览器访问 `http://0.0.0.0:8080`
2. 关于页面
    1. 默认路径为显示欢迎页面: `Welcome to Youwant Platform`
    2. 访问 `/clock` 将得到一个拟态时钟
    2. 访问 `/fileupload` 将得到一个文件上传服务
    2. 访问 `/npmsearch` 将得到一个NPM包搜索服务
    2. 访问 `/pastebin` 将得到一个片段存储服务