package npmsearch

import (
	"github.com/gin-contrib/static"
	"github.com/gin-gonic/gin"

	"gitee.com/zinface/go.youwant-platform/extensions"
)

var Engine *gin.Engine
var Router *gin.RouterGroup

func RegisterServer(r *gin.Engine) {
	Engine = r
	Engine.LoadHTMLGlob("templates/*.html")
	extensions.RegisterHander(Engine, static.Serve("/npmsearch/assets", static.LocalFile("./static/assets", false)))

	Router = Engine.Group("/npmsearch")
	{
		Router.GET("/", index)
		Router.GET("/search", search)
	}
}
