package extensions

import "github.com/gin-gonic/gin"

var RouterHandler []gin.HandlerFunc

func RegisterHander(r *gin.Engine, h gin.HandlerFunc) {
	RouterHandler = append(RouterHandler, h)
	r.Use(RouterHandler...)
}
