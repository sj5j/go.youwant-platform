package pastebin

import (
	"github.com/gin-contrib/static"
	"github.com/gin-gonic/gin"

	"gitee.com/zinface/go.youwant-platform/extensions"
)

var Engine *gin.Engine
var Group *gin.RouterGroup

func RegisterServer(r *gin.Engine) {
	Engine = r
	Engine.LoadHTMLGlob("templates/*.html")
	extensions.RegisterHander(Engine, static.Serve("/pastebin/assets", static.LocalFile("./static/assets", false)))

	Group = r.Group("/pastebin")
	{
		Group.GET("/", index)
		// Group.GET("/create", create)

		Group.POST("/api/create", apiCreate)
		Group.GET("/api/update", apiUpdate)
		Group.GET("/api/delete/:pasteuuid", apiDelete)

		Group.GET("/api/all", apiAll)
		Group.GET("/api/show/:pasteuuid", apiShow)
		Group.GET("/api/syntax", apiSyntax)
		Group.GET("/api/themes", apiThemes)
		Group.GET("/api/uuid", apiUuid)

	}
}

// var DefaultRouter = routers.Router{
// 	Path: "/pastebin",
// 	Mappings: []mappings.Mapping{
// 		mappings.CreateJson("/", defaultPasteJson),
// 		mappings.CreateJson("/_/new", newPaste),
// 		mappings.CreateString("/p/:id", showPaste),
// 	},
// }
