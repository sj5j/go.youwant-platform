package pastebin

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"

	models "gitee.com/zinface/go.youwant-platform/database"
)

func apiAll(c *gin.Context) {
	ps := models.FindPastebins()

	c.JSON(http.StatusOK, ps)
}

func index(c *gin.Context) {
	// c.JSON(http.StatusOK, "")
	c.HTML(http.StatusOK, "pastebin.html", gin.H{})
}

func create(c *gin.Context) {
	c.HTML(http.StatusOK, "pastebin-create.html", gin.H{})
}

func apiCreate(c *gin.Context) {
	title := c.PostForm("title")
	content := c.PostForm("content")
	author := c.PostForm("author")
	tags := c.PostForm("tags")
	syntax := c.PostForm("syntax")
	themes := c.PostForm("themes")

	if title == "" || content == "" {
		c.JSON(http.StatusOK, gin.H{
			"code":    http.StatusBadRequest,
			"message": "title or content is empty",
		})
		return
	}

	if author == "" {
		author = "unknow"
	}

	if syntax == "" {
		syntax = ""
	}

	if themes == "" {
		themes = "default.css"
	}

	fmt.Println("title", title)
	fmt.Println("content", content)
	fmt.Println("author", author)
	fmt.Println("syntax", syntax)
	fmt.Println("themes", themes)
	fmt.Println("tags", tags)

	paste := models.NewPastebin(title, content, author, tags, syntax, themes)
	paste.Save()

	c.JSON(http.StatusOK, gin.H{
		"code":    http.StatusOK,
		"message": "success",
	})
}

func apiUpdate(c *gin.Context) {
	title := c.PostForm("title")
	content := c.PostForm("content")
	author := c.PostForm("author")
	tags := c.PostForm("tags")
	syntax := c.PostForm("syntax")
	themes := c.PostForm("themes")
	uuid := c.PostForm("uuid")

	if uuid == "" {
		c.JSON(http.StatusOK, gin.H{
			"code":    http.StatusInternalServerError,
			"message": "uuid is empty",
		})
		return
	}

	if title == "" || content == "" {
		c.JSON(http.StatusOK, gin.H{
			"code":    http.StatusBadRequest,
			"message": "title or content is empty",
		})
		return
	}

	if author == "" {
		author = "unknow"
	}

	if themes == "" {
		themes = "assets/styles/default.css"
	}

	fmt.Println("title", title)
	fmt.Println("content", content)
	fmt.Println("author", author)
	fmt.Println("syntax", syntax)
	fmt.Println("themes", themes)
	fmt.Println("tags", tags)

	paste := models.NewPastebin(title, content, author, tags, syntax, themes)
	paste.Save()

	c.JSON(http.StatusOK, gin.H{
		"code":    http.StatusOK,
		"message": "success",
	})
}

func apiDelete(c *gin.Context) {
	pasteuuid := c.Param("pasteuuid")

	paste, err := models.FindPastebinUUID(pasteuuid)
	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"status":  http.StatusInternalServerError,
			"message": fmt.Sprintf("Not Found Paste: %v", err),
		})
		return
	}

	if err := paste.Delete(); err != nil {
		c.JSON(http.StatusOK, gin.H{
			"status":  http.StatusInternalServerError,
			"message": fmt.Sprintf("Paste Delete Failed: %v", err.Error()),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"status":  http.StatusOK,
		"message": "Paste delete successfully",
	})

}

func apiShow(c *gin.Context) {
	pasteuuid := c.Param("pasteuuid")

	if pasteuuid == "" {
		c.String(http.StatusBadRequest, "pasteid is required")
		return
	}

	paste, err := models.FindPastebinUUID(pasteuuid)
	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"code":    http.StatusNotFound,
			"message": fmt.Sprintf("Not Found UUID: %v", pasteuuid),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code":    http.StatusOK,
		"message": "success",
		"paste":   paste,
	})
}

func apiSyntax(c *gin.Context) {

	syntaxs, err := models.FindPastebinSyntax()
	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"status":  http.StatusInternalServerError,
			"message": fmt.Sprintf("Not Found Syntaxs"),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"status":  http.StatusOK,
		"message": fmt.Sprintf("Found Syntaxs"),
		"syntaxs": syntaxs,
	})
}

func apiThemes(c *gin.Context) {
	themes, err := models.FindPastebinThemes()
	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"status":  http.StatusInternalServerError,
			"message": fmt.Sprintf("Not Found Themes"),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"status":  http.StatusOK,
		"message": fmt.Sprintf("Found Themes"),
		"themes":  themes,
	})

}

func apiUuid(c *gin.Context) {
	c.String(http.StatusOK, models.GeneratePastebinUUID())
}

/****************************************************************






****************************************************************/
