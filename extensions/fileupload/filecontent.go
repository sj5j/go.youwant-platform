package fileupload

import (
	"net/http"
	"os"
)

func GetFileContentType(out *os.File) (string, error) {
	var contentType string

	buf := make([]byte, 512)
	_, err := out.Read(buf)
	if err != nil {
		return "", err
	}
	defer out.Close()

	contentType = http.DetectContentType(buf)

	return contentType, nil
}

func GetFileContentTypeFromPath(path string) (string, error) {
	f, err := os.Open(path)
	if err != nil {
		panic(err)
	}
	return GetFileContentType(f)
}
