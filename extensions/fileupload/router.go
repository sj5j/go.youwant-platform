package fileupload

import (
	"github.com/gin-contrib/static"
	"github.com/gin-gonic/gin"

	"gitee.com/zinface/go.youwant-platform/extensions"
)

var Engine *gin.Engine
var Group *gin.RouterGroup

func RegisterServer(r *gin.Engine) {
	Engine = r
	Group = r.Group("/fileupload")
	extensions.RegisterHander(Engine, static.Serve("/fileupload/assets", static.LocalFile("./static/assets", false)))
	{
		Group.GET("/", index)
		Group.GET("/old", oldindex)
		Group.GET("/download/:fileuuid", download)
		Group.POST("/upload", upload)

		Api := Group.Group("/api")
		{
			Api.GET("/files", apiAll)
			Api.POST("/upload", apiUpload)
			Api.GET("/cleanbad", apiCleanBadFiles)
			Api.GET("/delete/:fileuuid", apiDelete)
		}
	}
}
