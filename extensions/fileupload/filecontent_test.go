package fileupload

import (
	"fmt"
	"testing"
)

func TestGetFileContentTypeFromPath(t *testing.T) {
	tests := []struct {
		Name string
		Path string
	}{
		{
			Name: "test",
			Path: "filecontent.go",
		},
	}
	for _, tt := range tests {
		t.Run(tt.Name, func(t *testing.T) {
			contentType, err := GetFileContentTypeFromPath(tt.Path)
			if err != nil {
				t.Errorf("GetFileContentTypeFromPath: %v", err)
			}
			fmt.Printf("tt.Path: %v -- ", tt.Path)
			fmt.Printf("contentType: %v\n", contentType)
			// got, err := GetFileContentTypeFromPath(tt.args.path)
			// if (err != nil) != tt.wantErr {
			// 	t.Errorf("GetFileContentTypeFromPath() error = %v, wantErr %v", err, tt.wantErr)
			// 	return
			// }
			// if got != tt.want {
			// 	t.Errorf("GetFileContentTypeFromPath() = %v, want %v", got, tt.want)
			// }
		})
	}
}
