package fileupload

import (
	"fmt"
	"os"
	"path/filepath"
	"time"

	models "gitee.com/zinface/go.youwant-platform/database"
	"gitee.com/zinface/go.youwant-platform/external/utils"
)

// Item 组织成一条数据
// d791ad66c8e24b6db0879e4f0ed19c0d                    2022-05-31 11:08:25     5.32K   t.c
type Item struct {
	Uri        string    // ?
	UriSuffix  int       // 52
	Name       string    //
	Time       time.Time // 17
	TimeSuffix int       // 8
	Size       string    // ?
}

func createItem(uri string, name string, time time.Time, size string) Item {
	return Item{
		Uri:        uri,
		UriSuffix:  (52 - len(uri)),
		Name:       name,
		Time:       time,
		TimeSuffix: (10 - len(size)),
		Size:       size,
	}
}

func createSpace(length int) string {
	var buffer []byte
	for i := 0; i < length; i++ {
		buffer = append(buffer, ' ')
	}
	return string(buffer)
}

func (t *Item) UriSuffixString() string {
	return createSpace(t.UriSuffix)
}
func (t *Item) TimeSuffixString() string {
	return createSpace(t.TimeSuffix)
}

func (t *Item) String() string {
	return fmt.Sprintf("%s%s%s%s%s", t.Uri, t.UriSuffixString(), t.Time.Format("2006-01-02 15:04:05"), t.TimeSuffixString(), t.Size)
}

func (t Item) Url() string {
	var url = fmt.Sprintf("<a href=\"download/%s\">%s</a>", t.Uri, t.Uri)
	return fmt.Sprintf("%s%s%s%s%s   %s", url, t.UriSuffixString(), t.Time.Format("2006-01-02 15:04:05"), t.TimeSuffixString(), t.Size, t.Name)
}

func FileUploadUrl(fu *models.FileUpload) string {
	info, _ := os.Stat(filepath.Join(BASE_PATH, fu.Uuid+fu.FileExt))
	return createItem(fu.Uuid, fu.FileName, info.ModTime(), utils.FormatFileSize(info.Size())).Url()
}

func FileUploadItem(fu *models.FileUpload) Item {
	info, _ := os.Stat(filepath.Join(BASE_PATH, fu.Uuid+fu.FileExt))
	return createItem(fu.Uuid, fu.FileName, info.ModTime(), utils.FormatFileSize(info.Size()))
}

// func main() {

// 	var items = createItem("fuck", time.Now(), "100K")

// 	fmt.Println(items.String())
// 	fmt.Println(items.Url())
// }
