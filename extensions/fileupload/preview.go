package fileupload

var pvs []string

var pvMaxsize int64

func init() {

	// previewsize < 5mb
	pvMaxsize = 5 * 1024 * 1024

	pvs = []string{
		".pdf",
		".jpg", ".png",
	}
}
