package fileupload

import (
	"path/filepath"

	filemanager "gitee.com/zinface/go.gin-filemanager"
)

const (
	BASE_PATH = "./static/upload/"
)

type File struct {
	Id   string `json:"id,omitempty"`
	Name string `json:"name,omitempty"`
	Ext  string `json:"ext,omitempty"`
	Md5  string `json:"md5,omitempty"`
	UUID string `json:"uuid,omitempty"`
}

type Files []File

var fm *filemanager.FileManager
var FileUpload Files

func init() {
	fm = filemanager.NewFileManager(BASE_PATH)
	FileUpload = Files{
		File{
			Id:   "dog",
			Name: "dog.jpg",
			Ext:  ".jpg",
			UUID: "dog",
		},
	}
}

func GetSavePath(path string) string {
	return filepath.Join(BASE_PATH, path)
}
