package models

import "strings"

// PastebinSyntax 语法结构
type PastebinSyntax struct {
	Id     int    `json:"id"`
	Syntax string `json:"syntax"`
	Value  string `json:"value"`
}

type PastebinSyntaxs []PastebinSyntax

// SyntaxAdjust 可能包含多个语法项，调整为保留一项进行标识
func SyntaxAdjust(ps *PastebinSyntaxs) PastebinSyntaxs {
	if ps == nil {
		return nil
	}

	var adjust = []PastebinSyntax{}
	for _, p := range *ps {
		var v = strings.Split(p.Value, ",")
		if len(v) == 1 {
			adjust = append(adjust, p)
			continue
		}
		p.Value = strings.TrimSpace(v[0])
		adjust = append(adjust, p)
	}
	return adjust
}

// FindPastebinSyntax 从数据库中查询所有语法
func FindPastebinSyntax() (PastebinSyntaxs, error) {
	var syntaxs PastebinSyntaxs
	err := db.Find(&syntaxs).Error
	return SyntaxAdjust(&syntaxs), err
}
