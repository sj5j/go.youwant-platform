package models

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"

	"gitee.com/zinface/go.youwant-platform/core"
)

type DBConfig struct {
	Dialect string
	Path    string
}

var db *gorm.DB

func Setup() {
	cfg, err := core.DatabaseConfig()
	if err != nil {
		panic(err)
	}

	db, err = gorm.Open(cfg.Dialect, cfg.Path)
	if err != nil {
		panic(err)
	}

	// db.AutoMigrate(&....{})
	db.AutoMigrate(&FileUpload{})
	db.AutoMigrate(&Pastebin{})
	db.AutoMigrate(&PastebinSyntax{})
	db.AutoMigrate(&PastebinThemes{})

	initializeSyntax()
	initializeThemes()
}
