package models

// PastebinThemes 主题结构
type PastebinThemes struct {
	Id    int    `json:"id"`
	Name  string `json:"name"`
	Theme string `json:"theme"`
}

// FindPastebinThemes 从数据库中查询所有主题
func FindPastebinThemes() ([]PastebinThemes, error) {
	var themes []PastebinThemes
	return themes, db.Find(&themes).Error
}
