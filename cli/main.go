package main

import (
	"fmt"
	"net/http"

	"github.com/gin-contrib/static"
	"github.com/gin-gonic/gin"

	"gitee.com/zinface/go.youwant-platform/core"
	models "gitee.com/zinface/go.youwant-platform/database"
	"gitee.com/zinface/go.youwant-platform/extensions/fileupload"
	"gitee.com/zinface/go.youwant-platform/extensions/npmsearch"
	"gitee.com/zinface/go.youwant-platform/extensions/pastebin"
)

func main() {
	fmt.Println("Hello: Youwant Platform")
	models.Setup()

	r := gin.Default()

	r.LoadHTMLGlob("templates/*.html")
	r.Use(static.Serve("/assets", static.LocalFile("./static/assets", false)))

	r.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.html", gin.H{})
	})

	r.GET("/clock", func(c *gin.Context) {
		c.HTML(http.StatusOK, "clock.html", gin.H{})
	})

	fileupload.RegisterServer(r)
	npmsearch.RegisterServer(r)
	pastebin.RegisterServer(r)

	// 基于配置与默认启动端口
	cfg, err := core.SettingConfig()
	if err != nil {
		r.Run("0.0.0.0:8080")
	} else {
		r.Run(fmt.Sprintf("%v:%v", cfg.Host, cfg.Port))
	}
}

// 执行 `go run .`，使用浏览器访问 `http://0.0.0.0:8080`
