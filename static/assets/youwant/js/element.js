function createElement(name) {
    return document.createElement(name);
}

function createElementAfter(name, after) {
    let element = createElement(name);
    after(element)
    return element
}

function createElementInnerHTML(name, html) {
    return createElementAfter(name, (element)=>{
        element.innerHTML = html
    });
}

function createElementValue(name, value) {
    return createElementAfter(name, (element)=>{
        element.value = value
    })
}

function createElementAppendChild(name, child) {
    return createElementAfter(name, (element)=>{
        element.appendChild = child
    })
}