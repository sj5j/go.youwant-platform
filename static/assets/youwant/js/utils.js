/**
 * 注册切换按钮事件 - 适用于 bootstrap collapse 响应操作
 * @param {string|object} source 切换源
 * @param {string|object} target 切换目标
 * @param {boolean} toggle 初始值
 * @param {func} toggle_after 后续处理函数
 */
function registerToggleButtonAfter(source, target, toggle, toggle_after) {
    var toggle_menu = toggle;
    $(source).click(()=>{
        if ((toggle_menu=!toggle_menu)) {
            $(target).addClass("in")
            $(source).html("隐藏目录")
        } else {
            $(target).removeClass("in")
            $(source).html("显示目录")
        }
        if (toggle_after !== undefined) {
            toggle_after(toggle_menu)
        }
    })
}

/**
 * 注册事件处理器 - 基于 jQuery $.on 统一操作
 * @param {any} target 事件注册源
 * @param {string} signal 事件触发器
 * @param {func} callback 槽函数
 */
function registerEventHandler(target, signal, callback) {
    $(target).on(signal, callback)
}

// Utils
// containsAfter("key key key", [{}], "Name", function([], i))
/**
 * 注册数据列表搜索引擎
 * @param {string} value 搜索值
 * @param {[{}]} datalist 数据集
 * @param {string} key 关键属性
 * @param {callback} after 后续处理器
 */
function valueListKeySearchAfter(value, datalist, key, after) {
    let ks = String(value).toLocaleLowerCase().split(" ")

    for (let i = 0; i < all.length; i ++) {
        let has = true
        for (let j = 0; j < ks.length; j++) {
            const k = ks[j];
            if (String(all[i][key]).toLowerCase().search(k) == -1) {
                has = false;
            }
        }
        if (has) {
            after(all, i)
        }
    }
}
