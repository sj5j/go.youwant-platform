package core

import "github.com/go-ini/ini"

const (
	ConfigFilePath = "config/config.ini"
)

func LoaderDefaultConfig(section string, v interface{}) error {
	cfg, err := ini.Load(ConfigFilePath)
	if err != nil {
		return err
	}
	return cfg.Section(section).MapTo(v)
}
