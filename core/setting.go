package core

type Settings struct {
	Host string
	Port int
	Mode string
}

// SettingsConfig 获取服务配置
func SettingConfig() (*Settings, error) {
	var s = &Settings{}
	err := LoaderDefaultConfig("Setting", s)
	return s, err
}
