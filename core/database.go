package core

type Database struct {
	Dialect string
	Path    string
}

func DatabaseConfig() (*Database, error) {
	var d = &Database{}
	err := LoaderDefaultConfig("Database", d)
	return d, err
}
